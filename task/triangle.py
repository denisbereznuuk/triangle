def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    are_numbers = ((isinstance(a, (int, float))) and (isinstance(b, (int, float))) and (isinstance(c, (int, float))))
    return are_numbers and (a > 0 and b > 0 and c > 0) and (a + b > c and a + c > b and b + c > a)
